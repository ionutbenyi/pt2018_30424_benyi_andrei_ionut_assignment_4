package bank.pt2018_30424_benyi_andrei_ionut_assignment_4;

import static org.junit.Assert.*;

import org.junit.Test;

import functionalP.Person;
import functionalP.SpendingsAccount;
public class WithdrawTest {

	@Test
	public void testWithdrawal() {
		Person person=new Person(5,"Mihai","Andrei","Obs 5",25,"12345");
		SpendingsAccount s=new SpendingsAccount(3,person);
		s.addMoney(10000);
		if(s.withdraw(2000)==-1) {
			System.exit(0);
		}
		else {
			assertEquals(s.getBallance(),8000);
		}
	}

}
