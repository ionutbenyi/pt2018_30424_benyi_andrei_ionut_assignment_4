package bank.pt2018_30424_benyi_andrei_ionut_assignment_4;

import junit.framework.TestCase;
import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import org.junit.Test;

import functionalP.Person;

public class SerializeTest extends TestCase {
	
	@Test
	public void testSerialization() {
		ArrayList<Person> persons=new ArrayList<Person>();
		Person person=new Person(5,"Mihai","Andrei","Obs 5",25,"12345");
		//deserialize
		try {
			FileInputStream fin=new FileInputStream("persons.ser");
			ObjectInputStream in=new ObjectInputStream(fin);
			persons=(ArrayList<Person>) in.readObject();
			in.close();
			fin.close();
		}catch(IOException e2) {
			e2.printStackTrace();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		
		persons.add(person);
				
		//serialize
		try {
			FileOutputStream fout=new FileOutputStream("persons.ser");
			ObjectOutputStream out=new ObjectOutputStream(fout);
			out.writeObject(persons);
			out.close();
			fout.close();
		}catch(IOException exc) {
			exc.printStackTrace();
		}
		
		//deserialize
				try {
					FileInputStream fin2=new FileInputStream("persons.ser");
					ObjectInputStream in2=new ObjectInputStream(fin2);
					persons=(ArrayList<Person>) in2.readObject();
					in2.close();
					fin2.close();
				}catch(IOException e2) {
					e2.printStackTrace();
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
				
	assertEquals(person.getId(),persons.get(persons.size()-1).getId());
				
	}
}
