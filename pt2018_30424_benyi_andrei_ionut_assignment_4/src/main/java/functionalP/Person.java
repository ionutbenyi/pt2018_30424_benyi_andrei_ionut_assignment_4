package functionalP;
import java.util.*;
import java.awt.*;
import java.io.Serializable;
/**
 * The class which handles the objects of type Person
 * @author Andrei Ionut Benyi
 *
 */
public class Person implements Serializable{
	
	private int id;
	private String address;
	private String firstName;
	private String lastName;
	private String password;
	private int age;
	private ArrayList<Account> accounts;
	
	public Person(int i,String fn, String ln,String addr, int a,String p) {
		this.id=i;
		this.address=addr;
		this.firstName=fn;
		this.lastName=ln;
		this.age=a;
		this.password=p;
		this.accounts=new ArrayList<Account>();
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int i) {
		this.id=i;
	}
	
	public String getAddress() {
		return this.address;
	}
	
	public void setAddress(String s) {
		this.address=s;
	}
	
	public String getFirstName() {
		return this.firstName;
	}
	
	public void setFirstName(String s) {
		this.firstName=s;
	}
	
	public String getLastName() {
		return this.lastName;
	}
	
	public void setLastName(String s) {
		this.lastName=s;
	}
	
	public int getAge() {
		return this.age;
	}
	
	public void setAge(int i) {
		this.age=i;
	}
	
	public void addAccount(Account a) {
		this.accounts.add(a);
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public int getAccNr() {
		return this.accounts.size();
	}
	/**
	 * @pre id>=0;
	 * @param id
	 * @post size2==size2-1;
	 */
	public void deleteAccount(int id) {
		assert id>=0;
		
		Account r=null;
		int size=this.accounts.size();
		for(int i=0;i<size;i++) {
			if(this.accounts.get(i).getNumber()==id)
				r=this.accounts.get(i);
		}
		this.accounts.remove(r);
		int size2=this.accounts.size();
		
		assert size2==size-1;
	}
	
	public void withdraw(int amount, int id) {
		for(int i=0;i<this.accounts.size();i++) {
			if(this.accounts.get(i).getNumber()==id) {
				if(this.accounts.get(i).withdraw(amount)==-1) {
					return;
				}
			}
		}
	}
	
	public void addMoney(int amount, int id) {
		for(int i=0;i<this.accounts.size();i++) {
			if(this.accounts.get(i).getNumber()==id) {
				this.accounts.get(i).addMoney(amount);
			}
		}
	}
}
