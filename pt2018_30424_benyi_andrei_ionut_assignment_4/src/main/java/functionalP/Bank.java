package functionalP;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Hashtable;
/**
 * The class which represents the bank management
 * @invariant notNull()
 * @author Andrei Ionut Benyi
 *
 */
public class Bank implements BankProc, Serializable{
	
	public HashMap<Integer,Account> acc;
	
	public Bank( HashMap<Integer,Account> ac) {
		this.acc=ac;
	}
	
	public boolean notNull() {
		return this.acc.size()>=0;
	}
	
	public int getNrAccounts() {
		return this.acc.size();
	}
	/**
	 * @pre a!=null
	 * @post size2==size+1
	 */
	public void newAccount(int i,Account a) {
		assert a!=null;
		assert notNull();
		
		int size=getNrAccounts();
		this.acc.put(i, a);
		
		int size2=getNrAccounts();
		assert size2==size+1;
		assert notNull();
	}

	/**
	 * @pre id>=0
	 * @post size2=size-1
	 */
	public void deleteAccount(int id) {
		assert id>=0;
		assert notNull();
		
		int size=getNrAccounts();
		acc.remove(id);
		int size2=getNrAccounts();
		
		assert size2==size-1;
		assert notNull();
	}
	
	
}
