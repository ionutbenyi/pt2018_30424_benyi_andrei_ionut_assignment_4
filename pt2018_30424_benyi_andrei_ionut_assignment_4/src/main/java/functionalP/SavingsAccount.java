package functionalP;

import java.io.Serializable;

import javax.swing.JOptionPane;
/**
 * The subclass of the Account class - the Savings account
 * @author Andrei Ionut Benyi
 *
 */
public class SavingsAccount extends Account implements Serializable {
	
	private final int dobanda=10;
	private int accessings;
	private final int min=100; //min account to withdraw
	private int totalWithdrawal;
	private int ballance;
	
	public SavingsAccount(int nr, Person h) {
		super(nr,h);
		this.ballance=0;
		this.totalWithdrawal=0;
		this.accessings=0;
	}
	
	@Override
	public void addMoney(int amount) {
		int res=this.getBallance()+amount;
		if(this.accessings<=5)
		{
			this.setBallance(res-(dobanda*res)/100);
			this.accessings=this.accessings+1;
		}
		else {
			JOptionPane.showMessageDialog(null, "The savings account accessed too many times!");
			accessings=0;
		}
	}
	
	public void resetAccessings() {
		this.accessings=0;
	}
	
	
	
}
