package functionalP;

import java.io.Serializable;

import javax.swing.JOptionPane;
/**
 * The subclass of the Account class - the Spending account
 * @author Andrei Ionut Benyi
 *
 */
public class SpendingsAccount extends Account implements Serializable {
	private int totalWithdrawal;
	public SpendingsAccount(int nr, Person h){
		super(nr,h);
		this.totalWithdrawal=0;
	}
	
	/**
	 * @pre amount >=0;
	 * @post this.getBallance()>=0
	 */
	@Override
	public void addMoney(int amount) {
		assert amount>=0;
		this.setBallance(this.getBallance()+amount);
		assert this.getBallance()>=0;
	}
	
	/**
	 * @pre amount<=ballance
	 * @post this.ballance>=0
	 */
	
}
