package functionalP;
import java.util.*;

import javax.swing.JOptionPane;

import java.awt.*;
import java.io.Serializable;
/**
 * The class which manages the operations on a single account
 * @author Andrei Ionut Benyi
 *
 */
public class Account implements Serializable{
	
	private int number;
	private int ballance;
	private Person holder;
	
	public Account(int nr, Person p) {
		this.ballance=0;
		this.number=nr;
		this.holder=p;
	}
	
	public int getNumber() {
		int i=this.number;
		return i;
		
	}
	
	/**
	 * @pre i>=0
	 * @param i
	 */
	public void setNumber(int i) {
		assert i>=0;
		this.number=i;
	}
	
	public Person getHolder() {
		return this.holder;
	}
	/**
	 * @pre p!=null;
	 * @param p
	 * @post this.holder==p;
	 */
	public void setHolder(Person p) {
		assert p!=null;
		this.holder=p;
		assert this.holder==p;
	}
	
	public int getBallance() {
		return this.ballance;
	}
	
	/**
	 * @pre i>=0
	 * @post this.ballance>=0
	 * @param i
	 */
	public void setBallance(int i) {
		assert i>=0;
		this.ballance=this.ballance+i; 
		assert this.ballance>=0;
	}
	
	/**
	 * @pre amount>=0
	 * @param amount
	 * @post this.ballance>=0;
	 */
	public void addMoney(int amount) {
		assert amount>=0;
		ballance=ballance+amount;
		assert this.ballance>=0;
	}
	
	/**
	 * @pre this.ballance >=amount
	 * @param amount
	 * @return
	 */
	public int withdraw(int amount) {
		assert this.ballance >=amount;
		
		if(ballance<amount) {
			return -1;
		}
		else {
			ballance=ballance-amount;
			return ballance;
		}
	}
	
	
}
