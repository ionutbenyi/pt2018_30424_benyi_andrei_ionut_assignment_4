package Presentation;
import functionalP.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class View1 extends JFrame implements Serializable{
	
	private JLabel id;
	public JTextField idTF;
	private JLabel firstName;
	public JTextField firstNameTF;
	private JLabel lastName;
	public JTextField lastNameTF;
	private JLabel pass;
	public JTextField passTF;
	
	private JButton log;
	private JButton newP; 
	private JButton delP;
	private JButton showP;
	public JFrame mainFr=new JFrame();
	
	private JPanel cp1;
	private JPanel cp2;
	private JPanel cp3;
	private JPanel cp4;
	private JPanel cp5;
	private JPanel cp6;
	public static JPanel cp7;
	
	public View1() {
		mainFr.setSize(500,500);
		mainFr.setLocationRelativeTo(null);
		mainFr.setLayout(new FlowLayout());
		
		id=new JLabel("ID");
		idTF=new JTextField("");
		firstName=new JLabel("First Name");
		firstNameTF=new JTextField("");
		lastName=new JLabel("Last Name");
		lastNameTF=new JTextField("");
		pass=new JLabel("Password");
		passTF=new JTextField("");
		
		log=new JButton("Log in");
		delP=new JButton("Delete person");
		newP=new JButton("New Person");
		showP=new JButton("Show Persons");
	
		cp1=new JPanel();
		cp1.setLayout(new BoxLayout(cp1,BoxLayout.PAGE_AXIS));
		cp1.add(id);
		cp1.add(idTF);
		
		cp2=new JPanel();
		cp2.setLayout(new BoxLayout(cp2,BoxLayout.PAGE_AXIS));
		cp2.add(firstName);
		cp2.add(firstNameTF);
		
		cp3=new JPanel();
		cp3.setLayout(new BoxLayout(cp3,BoxLayout.PAGE_AXIS));
		cp3.add(lastName);
		cp3.add(lastNameTF);
		
		cp4=new JPanel();
		cp4.setLayout(new BoxLayout(cp4,BoxLayout.PAGE_AXIS));
		cp4.add(pass);
		cp4.add(passTF);
		
		cp5=new JPanel();
		cp5.setLayout(new BoxLayout(cp5,BoxLayout.PAGE_AXIS));
		cp5.add(log);
		cp5.add(newP);
		cp5.add(delP);
		cp5.add(showP);
		
		cp6=new JPanel();
		cp6.setLayout(new BoxLayout(cp6,BoxLayout.PAGE_AXIS));
		cp6.add(cp1);
		cp6.add(cp2);
		cp6.add(cp3);
		cp6.add(cp4);
		cp6.add(cp5);
		
		cp7=new JPanel();
		
		mainFr.add(cp6);
		mainFr.add(cp7);
		mainFr.setVisible(true);
		
		//--------------------------------------------------------listeners
		log.addActionListener(new ActionListener() {
			
			public Person getPersonFromList(int id) {
				ArrayList<Person>persons=new ArrayList<Person>();
				
				//deserialize
				try {
					FileInputStream fin=new FileInputStream("persons.ser");
					ObjectInputStream in=new ObjectInputStream(fin);
					persons=(ArrayList<Person>) in.readObject();
					in.close();
					fin.close();
				}catch(IOException e2) {
					e2.printStackTrace();
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
				for(int i=0;i<persons.size();i++) {
					if(persons.get(i).getId()==id)
						return persons.get(i);
				}
				return null;
			}
			public void actionPerformed(ActionEvent e) {
				if(passTF.getText().equals((getPersonFromList(Integer.parseInt(idTF.getText()))).getPassword()))
				{
					View5 v5=new View5();
				}
				else {
					JOptionPane.showMessageDialog(null, "Error. Wrong Password");
				}
			}
		});
		
		newP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				View2 v2=new View2();
				
				ArrayList<Person> p=new ArrayList<Person>();
				try {
					FileInputStream fin=new FileInputStream("persons.ser");
					ObjectInputStream in=new ObjectInputStream(fin);
					p=(ArrayList<Person>) in.readObject();
					in.close();
					fin.close();
				}catch(IOException e2) {
					e2.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		
		showP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				View3 v3=new View3();
			}
		});
		
		delP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				View4 v4=new View4();
			}
		});
		//--------------------------------------------------------end listeners
	}
	
}
