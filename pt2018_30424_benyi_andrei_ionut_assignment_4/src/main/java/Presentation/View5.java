package Presentation;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.*;

import functionalP.Account;
import functionalP.Bank;
import functionalP.Person;
import functionalP.SavingsAccount;
import functionalP.SpendingsAccount;
public class View5 extends JFrame{
	private JFrame mainFr=new JFrame();
	
	private JLabel holderId;
	public JTextField holderIdTF;
	private JLabel holderPass;
	public JTextField holderPassTF;
	private JLabel accType;
	public JTextField accTypeTF;
	
	private JLabel amount;
	public JTextField amountTF;
	private JLabel number;
	public JTextField numberTF;

	private JButton sit;
	private JButton save;
	private JButton add;
	private JButton withdraw;
	private JButton delete;
	
	private JPanel cp1;
	private JPanel cp2;
	
	public View5() {
		mainFr.setSize(300,350);
		mainFr.setLocationRelativeTo(null);
		mainFr.setLayout(new FlowLayout());
		
		holderId=new JLabel("Holder ID");
		holderIdTF=new JTextField("");
		holderPass=new JLabel("Holder Pass");
		holderPassTF=new JTextField("");
		accType=new JLabel("Account type");
		accTypeTF=new JTextField("");
		amount=new JLabel("Amount");
		amountTF=new JTextField("");
		number=new JLabel("Account Number");
		numberTF=new JTextField("");
		
		sit=new JButton("Situation");
		save=new JButton("Save new");
		add=new JButton("Add");
		withdraw=new JButton("Withdraw");
		delete=new JButton("Delete");
		
		cp1=new JPanel();
		cp1.setLayout(new BoxLayout(cp1,BoxLayout.PAGE_AXIS));
		cp1.add(holderId);
		cp1.add(holderIdTF);
		cp1.add(holderPass);
		cp1.add(holderPassTF);
		cp1.add(accType);
		cp1.add(accTypeTF);
		cp1.add(amount);
		cp1.add(amountTF);
		cp1.add(number);
		cp1.add(numberTF);
		
		cp2=new JPanel();
		cp2.setLayout(new BoxLayout(cp2,BoxLayout.PAGE_AXIS));
		
		cp2.add(save);
		cp2.add(add);
		cp2.add(withdraw);
		cp2.add(delete);
		cp2.add(sit);
		
		mainFr.add(cp1);
		mainFr.add(cp2);
		mainFr.setVisible(true);
		
		//--------------------------------------------------------------------listeners
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(holderIdTF.getText().isEmpty() || holderPassTF.getText().isEmpty() || accTypeTF.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Error. Check parameters");
				}
				else {
					//bank deserialize
					HashMap<Integer,Account> hmap=new HashMap<Integer,Account>();
					Bank b=new Bank(hmap);
					try {
						FileInputStream fin=new FileInputStream("bank.ser");
						ObjectInputStream in=new ObjectInputStream(fin);
						//hmap=(HashMap<Integer,Account>) in.readObject();
						b=(Bank)in.readObject();
						in.close();
						fin.close();
					}catch(IOException e2) {
						e2.printStackTrace();
					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					
					
					//person deserialize
					ArrayList<Person>al=new ArrayList<Person>();
					try {
						FileInputStream fin=new FileInputStream("persons.ser");
						ObjectInputStream in=new ObjectInputStream(fin);
						al=(ArrayList<Person>) in.readObject();
						in.close();
						fin.close();
					}catch(IOException e2) {
						e2.printStackTrace();
					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					Person h=al.get(Integer.parseInt(holderIdTF.getText())-1);
					
					if((accTypeTF.getText()).equals("Spendings")) {
						SpendingsAccount sa=new SpendingsAccount(b.acc.size(),h);
						b.newAccount(b.acc.size(),sa);
					}
					else {
						if((accTypeTF.getText()).equals("Savings")) {
							SavingsAccount sa=new SavingsAccount(b.acc.size(),h);
							b.newAccount(b.acc.size(),sa);
							//b.acc.put(b.acc.size(), sa);
						}
						else {
							JOptionPane.showMessageDialog(null, "Error. No sunch account found");
						}
					}
					
					//serialize bank
					try {
						FileOutputStream fout=new FileOutputStream("bank.ser");
						ObjectOutputStream out=new ObjectOutputStream(fout);
						out.writeObject(b);
						out.close();
						fout.close();
					}catch(IOException exc) {
						exc.printStackTrace();
					}
					
				}
			}
		});
		
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(holderIdTF.getText().isEmpty() || holderPassTF.getText().isEmpty() || amountTF.getText().isEmpty() || numberTF.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Error. Check parameters");
				}
				else {
					//bank deserialize
					HashMap<Integer,Account> hmap=new HashMap<Integer,Account>();
					Bank b=new Bank(hmap);
					try {
						FileInputStream fin=new FileInputStream("bank.ser");
						ObjectInputStream in=new ObjectInputStream(fin);
						b=(Bank)in.readObject();
						in.close();
						fin.close();
					}catch(IOException e2) {
						e2.printStackTrace();
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					}
					
					int nr=Integer.parseInt(numberTF.getText());
					
					b.acc.get(nr).addMoney(Integer.parseInt(amountTF.getText()));
					
					//serialize bank
					try {
						FileOutputStream fout=new FileOutputStream("bank.ser");
						ObjectOutputStream out=new ObjectOutputStream(fout);
						out.writeObject(b);
						out.close();
						fout.close();
					}catch(IOException exc) {
						exc.printStackTrace();
					}
					
				}
			}
		});
		
		withdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(holderIdTF.getText().isEmpty() || holderPassTF.getText().isEmpty() || amountTF.getText().isEmpty() || numberTF.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Error. Check parameters");
				}
				else {
					
					//person deserialize
					ArrayList<Person>al=new ArrayList<Person>();
					try {
						FileInputStream fin=new FileInputStream("persons.ser");
						ObjectInputStream in=new ObjectInputStream(fin);
						al=(ArrayList<Person>) in.readObject();
						in.close();
						fin.close();
					}catch(IOException e2) {
						e2.printStackTrace();
					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					Person h=al.get(Integer.parseInt(holderIdTF.getText())-1);
					if((Integer.parseInt(holderIdTF.getText()))==h.getId()) {

						if((holderPassTF.getText()).equals(h.getPassword())) {
							//bank deserialize
							HashMap<Integer,Account> hmap=new HashMap<Integer,Account>();
							Bank b=new Bank(hmap);
							try {
								FileInputStream fin=new FileInputStream("bank.ser");
								ObjectInputStream in=new ObjectInputStream(fin);
								b=(Bank) in.readObject();
								in.close();
								fin.close();
							}catch(IOException e2) {
								e2.printStackTrace();
							} catch (ClassNotFoundException e1) {
								e1.printStackTrace();
							}
					
							int nr=Integer.parseInt(numberTF.getText());
					
							if(b.acc.get(nr).withdraw(Integer.parseInt(amountTF.getText()))==-1) {
								JOptionPane.showMessageDialog(null, "Insufficient amount");
							}
					
							//serialize bank
							try {
								FileOutputStream fout=new FileOutputStream("bank.ser");
								ObjectOutputStream out=new ObjectOutputStream(fout);
								out.writeObject(b);
								out.close();
								fout.close();
							}catch(IOException exc) {
								exc.printStackTrace();
							}
						}else {
							JOptionPane.showMessageDialog(null, "Wrong password!");
						}
					}else {
						JOptionPane.showMessageDialog(null, "No account with this nr assigned to this id");
					}
					
				}
			}
		});
		
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(holderIdTF.getText().isEmpty() || holderPassTF.getText().isEmpty() || numberTF.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Error. Check parameters");
				}
				else {
					//bank deserialize
					HashMap<Integer,Account> hmap=new HashMap<Integer,Account>();
					Bank b=new Bank(hmap);
					try {
						FileInputStream fin=new FileInputStream("bank.ser");
						ObjectInputStream in=new ObjectInputStream(fin);
						b=(Bank) in.readObject();
						in.close();
						fin.close();
					}catch(IOException e2) {
						e2.printStackTrace();
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					}
					
					b.deleteAccount(Integer.parseInt(numberTF.getText()));
					
					//serialize bank
					try {
						FileOutputStream fout=new FileOutputStream("bank.ser");
						ObjectOutputStream out=new ObjectOutputStream(fout);
						out.writeObject(b);
						out.close();
						fout.close();
					}catch(IOException exc) {
						exc.printStackTrace();
					}
					
				}
			}
		});
		
		sit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				View6 v6=new View6();
			}
		});
		//--------------------------------------------------------------------end listeners
		
	}
	
	
}
