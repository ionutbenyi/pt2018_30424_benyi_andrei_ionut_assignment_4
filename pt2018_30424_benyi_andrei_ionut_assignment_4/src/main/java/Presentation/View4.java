package Presentation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.*;

import functionalP.Person;

public class View4 extends JFrame{
	
	private JFrame mainFr2=new JFrame();
	private JLabel id;
	public static JTextField idTF;
	private JLabel firstName;
	public static JTextField firstNameTF;
	private JLabel lastName;
	public static JTextField lastNameTF;
	private JLabel address;
	public static JTextField addressTF;
	private JLabel age;
	public static JTextField ageTF;
	private JLabel pass;
	public static JTextField passTF;
	
	public JButton delete;
	
	private JPanel cp1;
	private JPanel cp2;
	private JPanel cp3;
	private JPanel cp4;
	private JPanel cp5;
	private JPanel cp6;
	private JPanel cp7;
	
	public View4() {
		mainFr2.setSize(300,350);
		mainFr2.setLocationRelativeTo(null);
		mainFr2.setLayout(new FlowLayout());
		cp1=new JPanel();
		cp1.setLayout(new BoxLayout(cp1,BoxLayout.PAGE_AXIS));
		
		id=new JLabel("ID");
		idTF=new JTextField("");
		idTF.setSize(new Dimension(10,100));
		firstName=new JLabel("First Name");
		firstNameTF=new JTextField("");
		firstNameTF.setSize(new Dimension(10,100));
		lastName=new JLabel("Last Name");
		lastNameTF=new JTextField("");
		lastNameTF.setSize(new Dimension(10,100));
		address=new JLabel("Address");
		addressTF=new JTextField("");
		addressTF.setSize(new Dimension(10,100));
		age=new JLabel("Age");
		ageTF=new JTextField("");
		ageTF.setSize(new Dimension(10,100));
		pass=new JLabel("Password");
		passTF=new JTextField("");
		passTF.setSize(new Dimension(10,100));
		
		delete=new JButton("Delete");
		
		cp1.add(id);
		cp1.add(idTF);
		cp1.add(firstName);
		cp1.add(firstNameTF);
		cp1.add(lastName);
		cp1.add(lastNameTF);
		
		cp1.add(pass);
		cp1.add(passTF);
		cp1.add(delete);
		
		mainFr2.add(cp1);
		mainFr2.setVisible(true);
		
		//--------------------------------------------listeners
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(firstNameTF.getText().isEmpty() || lastNameTF.getText().isEmpty() || passTF.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Error. Check parameters");
				}
				else {
					
					String firstName=firstNameTF.getText();
					String lastName=lastNameTF.getText();
					String pass=passTF.getText();
					
					ArrayList<Person> persons=new ArrayList<Person>();
					
					//deserialize
					try {
						FileInputStream fin=new FileInputStream("persons.ser");
						ObjectInputStream in=new ObjectInputStream(fin);
						persons=(ArrayList<Person>) in.readObject();
						in.close();
						fin.close();
					}catch(IOException e2) {
						e2.printStackTrace();
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					}
					
					int id=Integer.parseInt(idTF.getText());
					persons.remove(id-1);
					
					//serialize
					try {
						FileOutputStream fout=new FileOutputStream("persons.ser");
						ObjectOutputStream out=new ObjectOutputStream(fout);
						out.writeObject(persons);
						out.close();
						fout.close();
					}catch(IOException exc) {
						exc.printStackTrace();
					}
					
				}
			}
		});
		//--------------------------------------------end listeners
		
	}
}
