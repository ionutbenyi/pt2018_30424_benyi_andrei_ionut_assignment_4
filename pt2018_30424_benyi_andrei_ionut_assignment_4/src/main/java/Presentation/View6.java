package Presentation;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import functionalP.Account;
import functionalP.Bank;
import functionalP.Person;
import functionalP.SpendingsAccount;

import java.util.*;
public class View6 extends JFrame{
	public static JTable persons1=new JTable();
	public JFrame mainFr=new JFrame();
	
	
	public View6() {
		
		mainFr.setSize(900,300);
		mainFr.setLocationRelativeTo(null);
		mainFr.setLayout(new FlowLayout());
		mainFr.setVisible(true);
		HashMap<Integer,Account> hmap=new HashMap<Integer,Account>();
		Bank b=new Bank(hmap);
		ArrayList<Account> arAc=new ArrayList<Account>();
		//deserialize
		try {
			FileInputStream fin=new FileInputStream("bank.ser");
			ObjectInputStream in=new ObjectInputStream(fin);
			//hmap=(HashMap<Integer,Account>) in.readObject();
			b=(Bank)in.readObject();
			
			for(int i=0;i<b.acc.size();i++) {
				arAc.add(b.acc.get(i));
			}
			
			in.close();
			fin.close();
		}catch(IOException e2) {
			e2.printStackTrace();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		hmap=b.acc;
		
		String[] col={"Nr","Ballance","Holder1","Holder2"};
		persons1=new JTable(buildBankTable(hmap));
		persons1.setFillsViewportHeight(true);
		JScrollPane scrollPane=new JScrollPane(persons1);
		scrollPane.setPreferredSize(new Dimension(700,150));
		mainFr.add(scrollPane);
	}
	
	//----------------------------------------------------------build table
		public static DefaultTableModel buildBankTable(HashMap<Integer,Account>hmap) {
			//header
			Vector<String> col=new Vector<String>();
			col.add("Account Nr");
			col.add("Account type");
			col.add("First Name");
			col.add("Last Name");
			col.add("Balance");
			
			//contents of the table
			Vector<Vector<Object>> dat=new Vector<Vector<Object>>();
			
			//traverse the hash map using an iterator
			for(int i=0;i<hmap.size();i++) {
				Vector<Object> v = new Vector<Object>();
				if(hmap.get(i)!=null) {
					Account a = hmap.get(i);
					v.add(a.getNumber());
					String aT=new String();
					if(a instanceof SpendingsAccount) {
						aT=new String("Spendings Account");
					}
					else {
						aT=new String("Savings Account");
					}
					v.add(aT);
					v.add(a.getHolder().getFirstName());
					v.add(a.getHolder().getLastName());
					v.add(a.getBallance());
				
					dat.add(v);
				}
			}
			
			return new DefaultTableModel(dat,col);
			
		}
		//---------------------------------------------------------end build table
}
